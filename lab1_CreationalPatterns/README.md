# JavaDesignPatterns
Implementation of different design patterns in Java.

## Creational Patterns:
Creational patterns often used in place of direct instantiation with constructors. They make the creation process more adaptable and dynamic. In particular, they can provide a great deal of flexibility about which objects are created, how those objects are created, and how they are initialized.

| DESIGN PATTERN | PURPOSE |
| -------------- | :------ |
| Builder        | Builder design pattern is an alternative way to construct complex objects and should be used only when we want to build different types of immutable objects using same object building process. |
| Prototype      | Prototype design pattern is used in scenarios where application needs to create a large number of instances of a class, which have almost same state or differ very little. |
| Factory        | Factory design pattern is most suitable when complex object creation steps are involved. To ensure that these steps are centralized and not exposed to composing classes. |
| Abstract Factory| Abstract factory pattern is used whenever we need another level of abstraction over a group of factories created using factory pattern. |
| Singleton      | Singleton enables an application to have one and only one instance of a class per JVM. |

---

## Implementing Abstract Factory Pattern and Factory Method:
To implement this pattern I first created the interface *Vehicle*, which has the method **drive**.
Then I created two interfaces which implement *Vehicle*, *Moto* and *Car*. Each those interfaces have
two classes that implement them, *CarDev*, *CarProd*, *MotoDev*, *MotoProd*. Those classes extend the
*ClonableVehicles* class.
So the setup is done, because we want to isolate the classes *CarDev, CarProd, MotoDev,
MotoProd*, we will create the interface AbstractFactory, which will have the methods **createCar**
and **createMoto*. This interface will be implemented by the factory classes *ProductionFactory* and
*DevelopFactory*.
To isolate these classes even more, the class *Factory Producer* will be created. This class has the
method **getFactory** which will take as parameter a string - *dev* or *prod* and will return an instance
of the factory class we demand.
To test if everything works, the following test will be executed:

```java

@Test
    void checkFactory() {
        //dev
        FactoryProducer factoryProducer = new FactoryProducer();
        AbstractFactory devFactory = factoryProducer.getFactory("dev");

        Car devCar = devFactory.createCar("BMW", "Z4");
        devCar.drive();

        //prod
        AbstractFactory prodFactory = factoryProducer.getFactory("prod");

        Moto prodMoto = prodFactory.createMoto("BMW", "Z4");
        prodMoto.drive();
    }

```
### Abstract Factory

![](img/abstractfactori.JPG)


### Factory Method

![](img/factorymethod.JPG)

## Implementing Builder Pattern:
I implemented the builder pattern for the classes *CarDev, CarProd, MotoDev, MotoPro*, by
creating the classes *CarDevBuilder, CarProdBuilder, MotoDevBuilder, MotoProdBuilder*. Each class
has a constructor which takes the parameters name and model, getter and setters for the class fields:
*mName, mModel, mYear, mPrice, mDescription*; and the method that returns a new instance of the
class the builder is for.
To test if everything works, the following test will be executed:
```java
@Test
    void checkBuilder() {
        CarDev carDev1 = new CarDevBuilder("BMW", "Z4").setPrice(15000).setYear(2005).createCar();
        carDev1.drive();

        MotoProd motoProd = new MotoProdBuilder("BMW", "Z4").setPrice(15000).setYear(2005).createMoto();
        motoProd.drive();
    }
```
![](img/builderLab1.JPG)

## Implementing Prototype Pattern:
I implemented the prototype pattern using an object pool, I created the class *VehiclePool*,
which has the field **defaultVehicles**, the methods: **getVehicle** which has as parameter the index
of the vehicle we want, and returns a clone of the indicated vehicle, and the method **loadVehicles**
which populates the HashMap **defaultVehicles** with four predetermined values.
To test if everything works, the following test will be executed:
```java
@Test
 @Test
    void checkObjectPool() throws CloneNotSupportedException {
        VehiclePool vehiclePool = new VehiclePool();
        vehiclePool.loadDefaultCars();

        Assert.assertNotEquals(vehiclePool.getVehicle(1).hashCode(),vehiclePool.getVehicle(1).hashCode());

        vehiclePool.getVehicle(1).drive();
        vehiclePool.getVehicle(2).drive();
        vehiclePool.getVehicle(3).drive();
        vehiclePool.getVehicle(4).drive();

    }
```
![](img/prototype.JPG)

## Implementing Singleton Pattern:
I implemented this pattern by creating the class *ShopingCart*. If we are change the context of
out application to an internet store, it is logical that we only have one instance of a ShoppingCart
class. This class has the field **INSTANCE** which contains an instance of the class, the field **mVehicles**
which is a list of vehicles that the shopping cart contains, a constructor, the methods: *getInstance*
which returns the **INSTANCE** field of the class, *getVehicles* which returns the list of vehicles that are
in the cart, *addVehicle* which adds a vehicle to the list.
To test if everything works, the following test will be executed:
```java
    @Test
    void checkSingleton() throws CloneNotSupportedException {
        VehiclePool vehiclePool = new VehiclePool();
        vehiclePool.loadDefaultCars();

        ShoppingCart shoppingCart = ShoppingCart.getInstance();

        shoppingCart.addVehicle(vehiclePool.getVehicle(1));
        shoppingCart.addVehicle(vehiclePool.getVehicle(3));

        ShoppingCart shoppingCart2 = ShoppingCart.getInstance();
        Assert.assertEquals(shoppingCart.hashCode(),shoppingCart2.hashCode());

        System.out.println(shoppingCart.getVehicles().toString());


    }

```
![](img/singleton.JPG)

## Test Results
```
CarDev{name=’BMW’ , model=’Z4 ’ , description=’no Description ’ , year=2005, price=15000} − car dev drive
CarDev{name=’ Nissan ’ , model=’ Micra ’ , description=’no Description ’ , year=2008, price=6000} − car dev drive
CarDev{name=’ Suzuki ’ , model=’ Bandit ’ , description=’no Description ’ , year=2015, price=15000} − moto dev drive
CarDev{name=’Honda ’ , model=’ Hornet ’ , description=’no Description ’ , year=2010, price=4000} − moto dev drive
CarDev{name=’BMW’ , model=’Z4 ’ , description=’no Description ’ , year=2000, price=5000} − car dev drive
CarDev{name=’BMW’ , model=’Z4 ’ , description=’no Description ’ , year=2000, price=5000} − moto dev drive
CarDev{name=’BMW’ , model=’Z4 ’ , description=’no Description ’ , year=2005, price=15000} − car dev drive
CarDev{name=’BMW’ , model=’Z4 ’ , description=’no Description ’ , year=2005, price=15000} − moto dev drive
[ CarDev{name=’BMW’ , model=’Z4 ’ , description=’no Description ’ , year=2005, price=15000} , CarDev{name=’ Suzuki ’ , model=’ Bandit ’ , description=’no Description ’ , year=2015, price=15000}]


