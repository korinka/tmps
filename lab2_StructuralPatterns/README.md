# JavaDesignPatterns
Implementation of different design patterns in Java.

> Design patterns, are solutions for most commonly (and frequently) occurred problems while designing a software. These patterns are mostly _“evolved”_ rather than _“discovered”_. A lot of learning, by lots of professional, have been summarized into these design patterns. None of these patterns force you anything in regard to implementation; they are just guidelines to solve a particular problem – in a particular way – in particular contexts. Code implementation is your responsibility.


## Structural Patterns:
Structural design patterns are concerned with how classes and objects can be composed, to form larger structures. The structural design patterns **simplifies the structure by identifying the relationships**. These patterns focus on, how the classes inherit from each other and how they are composed from other classes.

| DESIGN PATTERN | PURPOSE |
| -------------- | :------ |
| Adapter        | Adapting an interface into another according to client expectation. |
| Bridge      | Separating abstraction (interface) from implementation. |
| Composite        | Allowing clients to operate on hierarchy of objects. |
| Decorator | Adding functionality to an object dynamically. |
| Facade      | Providing an interface to a set of interfaces. |

---

## Adapter Pattern:
This pattern is easy to understand as the real world is full of adapters. For example consider
a USB to Ethernet adapter. We need this when we have an Ethernet interface on one end and USB
on the other. Since they are incompatible with each other. we use an adapter that converts one
to other. This example is pretty analogous to Object Oriented Adapters. In design, adapters are
used when we have a class (Client) expecting some type of object and we have an object (Adaptee)
offering the same features but exposing a different interface.
To test if everything works, the following test will be executed:

```java
    @Test
    void checkAdapter() {
        ClientService clientService = new ClientService();
        System.out.println(clientService.getRandByVehicle(new VehiclePresentationImpl("BMW", "X6", new PresentatorCar())));
    }
```
![](img/adapter.JPG)

## Composite Pattern:

Composite design patten allows you to have a tree structure and ask each node in the tree
structure to perform a task.You can take real life example of a organization.It have general managers
and under general managers, there can be managers and under managers there can be developers.Now
you can set a tree structure and ask each node to perform common operation like getSalary().
As described by Gof:
”Compose objects into tree structure to represent part-whole hierarchies.Composite lets client
treat individual objects and compositions of objects uniformly”.
Composite design pattern treats each node in two ways-Composite or leaf.Composite means it
can have other objects below it.leaf means it has no objects below it.
To test if everything works, the following test will be executed:

```java
    @Test
    void checkComposite() {
        CarConcurentsObj carConcurents = new CarConcurentsObj("BMW", "3");
        carConcurents.addVehicle(new CarImpl("Mercedes", "C"));
        carConcurents.addVehicle(new CarImpl("Audi", "a4"));

        System.out.println(carConcurents.toString());
    }
```
![](img/composite.JPG)
## Decorator Pattern:

Decorator pattern allows a user to add new functionality to an existing object without altering
its structure. This type of design pattern comes under structural pattern as this pattern acts as a
wrapper to existing class.
This pattern creates a decorator class which wraps the original class and provides additional
functionality keeping class methods signature intact.
To test if everything works, the following test will be executed:

```java
    @Test
    void checkDecorat() {
        Vehicle vehicle = new CarImpl("Toyota","Prius");
        vehicle.drive();

        Vehicle sportCar = new SportDecorator(new CarImpl("BMW","M6"));
        sportCar.drive();

        Vehicle sportMoto = new ComfortDecorator(new MotoImpl("Mercedes","S-class"));
        sportMoto.drive();

    }
```
![](img/decorator.JPG)

## Facade Pattern:

Facade pattern hides the complexities of the system and provides an interface to the client
using which the client can access the system. This type of design pattern comes under structural
pattern as this pattern adds an interface to existing system to hide its complexities.
This pattern involves a single class which provides simplified methods required by client and
delegates calls to methods of existing system classes.
To test if everything works, the following test will be executed:

```java
    @Test
    void checkFacade() {
        VehicleMaker vehicleMaker = new VehicleMaker("New Brand", "New Model");
        vehicleMaker.driveCar();
        vehicleMaker.driveMoto();
    }
```
![](img/facede.JPG )

## Bridge Pattern:

Bridge is used when we need to decouple an abstraction from its implementation so that the
two can vary independently. This type of design pattern comes under structural pattern as this
pattern decouples implementation class and abstract class by providing a bridge structure between
them.
This pattern involves an interface which acts as a bridge which makes the functionality of
concrete classes independent from interface implementer classes. Both types of classes can be altered
structurally without affecting each other.
To test if everything works, the following test will be executed:

```java
    @Test
    void checkBridge() {
        VehiclePresentation vehicle = new VehiclePresentationImpl("BMW", "X6", new PresentatorCar());
        VehiclePresentation vehicle2 = new VehiclePresentationImpl("Mercedes", "E", new PresentatorMoto());

        vehicle.drive();
        vehicle2.drive();
    }
```
![](img/bridge.JPG)

## Test result


![](img/result.JPG)
