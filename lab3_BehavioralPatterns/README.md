# JavaDesignPatterns
Implementation of different design patterns in Java.

> Design patterns, are solutions for most commonly (and frequently) occurred problems while designing a software. These patterns are mostly _“evolved”_ rather than _“discovered”_. A lot of learning, by lots of professional, have been summarized into these design patterns. None of these patterns force you anything in regard to implementation; they are just guidelines to solve a particular problem – in a particular way – in particular contexts. Code implementation is your responsibility.


## Behavioral Patterns:
Behavioral patterns abstract an action we want to take on the object or class that takes the action. By changing the object or class, we can change the algorithm used, the objects affected, or the behavior, while still retaining the same basic interface for client classes.

---

## Memento Pattern:

Memento pattern is used to restore state of an object to a previous state. Memento pattern
falls under behavioral pattern category.

![](img/memento.JPG)

To test if everything works, the following test will be executed:

```java
    @Test
    void checkMemento() {
        BrandAnalitic brandAnalitic = new BrandAnalitic();

        Vehicle car = new CarImpl("BMW", "X6");
        Vehicle moto= new MotoImpl("Suzuki", "Bandit");

        brandAnalitic.add(car.saveBrandToMemento());
        brandAnalitic.add(moto.saveBrandToMemento());

        System.out.println(brandAnalitic.get(0).getName());
        System.out.println(brandAnalitic.get(1).getName());
    }

```

##  Object Null Pattern:

In Null Object pattern, a null object replaces check of NULL object instance. Instead of putting
if check for a null value, Null Object reflects a do nothing relationship. Such Null object can also be
used to provide default behavior in case data is not available.
In Null Object pattern, we create an abstract class specifying various operations to be done,
concrete classes extending this class and a null object class providing do nothing implementation of
this class and will be used seamlessly where we need to check null value.

![](img/nullO.JPG)

To test if everything works, the following test will be executed:


```java

    void checkNullObject() throws Exception {
        AbstractTransport transport1 = TransportFactory.getTransport("BMW", "x3");
        AbstractTransport transport2 = TransportFactory.getTransport("Opel", "Corsa");
        AbstractTransport transport3 = TransportFactory.getTransport("Audi", "A7");

        System.out.println(transport1.getName() + " " + transport1.getModel());
        System.out.println(transport2.getName() + " " + transport2.getModel());
        System.out.println(transport3.getName() + " " + transport3.getModel());

    }

```
## Observer Pattern:

Observer pattern is used when there is one-to-many relationship between objects such as if one
object is modified, its dependent objects are to be notified automatically. Observer pattern falls
under behavioral pattern category.

![](img/observer.JPG)

To test if everything works, the following test will be executed:

```java

    @Test
    void checkObserver() {
        Rulaj rulaj = new Rulaj();
        rulaj.setMeters(200000);

        new MetricObserver(rulaj);
        new ImperialObserver(rulaj);

        rulaj.setMeters(rulaj.getMeters() + 20000);


        rulaj.setMeters(rulaj.getMeters() + 50000);
    }

```
## State Pattern:

In State pattern a class behavior changes based on its state. This type of design pattern comes
under behavior pattern.
In State pattern, we create objects which represent various states and a context object whose
behavior varies as its state object changes.

![](img/state.JPG)

To test if everything works, the following test will be executed:

```java
    @Test
    void checkState() {
        ContextCarState contextCarState = new ContextCarState();

        RepairState repairState = new RepairState();
        repairState.doAction(contextCarState);

        System.out.println(contextCarState.getState().toString());

        TestingState testingState = new TestingState();
        testingState.doAction(contextCarState);

        System.out.println(contextCarState.getState().toString());
    }

```
## Template Pattern:

In Template pattern, an abstract class exposes defined way(s)/template(s) to execute its methods. Its sub-classes can override the method implementation as per need but the invocation is to be
in the same way as defined by an abstract class. This pattern comes under behavior pattern category.

![](img/template.JPG)

To test if everything works, the following test will be executed:

```java

    @Test
    void chechTemplate() {
        MotoImpl moto = new MotoImpl("Honda", "CBR");
        moto.play();
    }

```

## Test result

![](img/result.JPG)